# coding=utf-8
import logging
import os

from aiohttp import web

from db import close_dbs, init_dbs
from middlewares import cache_middleware_factory, cors_middleware_factory, errors_middleware_factory
from views import FactorialCalcView, FibonacciCalcView, ListView, PowerCalcView

logging.basicConfig(level=logging.DEBUG)


def get_server_app():
    app = web.Application()

    app.on_startup.append(init_dbs)
    app.on_cleanup.append(close_dbs)

    async def redirect_to_swagger(request):
        raise web.HTTPFound("http://0.0.0.0:81/")

    app.add_routes([
        web.route("GET", "/", redirect_to_swagger),
        web.view("/factorial", ListView),
        web.view("/factorial/{number}", FactorialCalcView),
        web.view("/fibonacci", ListView),
        web.view("/fibonacci/{number}", FibonacciCalcView),
        web.view("/power", ListView),
        web.view("/power/{base}/{exp}", PowerCalcView)
    ])

    if "redis" in app and app["redis"]:
        app.middlewares.append(cache_middleware_factory(["factorial", "fibonacci", "power"]))
    app.middlewares.append(cors_middleware_factory(["*"]))
    app.middlewares.append(errors_middleware_factory())

    return app


if __name__ == "__main__":
    DEBUG = os.environ.get("DEBUG")
    web.run_app(get_server_app(), port=8080 if DEBUG else 80)
