# coding=utf-8

import json
import logging
import os
from time import sleep
from unittest import TestCase

import requests

DEBUG = os.environ.get("DEBUG")
BASE_URL = "http://10.3.0.4"


def json_load(name=None):
    body = {}
    if name:
        with open(f"{'.' if DEBUG else ''}/test_json/{name.replace('/', '.')}") as file_handle:
            body = json.loads(file_handle.read())
    return body


def wait_for_rest_api():
    logging.info("Waiting for the REST API to be ready...")
    while True:
        try:
            # rsp = requests.head(f"{BASE_URL}/fibonacci/1", timeout=1)
            rsp = requests.get(f"{BASE_URL}/fibonacci/1", timeout=1)
        except requests.exceptions.ConnectionError:
            sleep(1.0)
        else:
            if rsp.status_code == 404:
                break
    logging.info("REST API is now ready...")


class MathsTestCase(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        wait_for_rest_api()

    @staticmethod
    def default_requests(url: str):
        url = url.strip("/")
        rsp = requests.get(f"{BASE_URL}/{url}")
        assert rsp.status_code == 404
        rsp = requests.post(f"{BASE_URL}/{url}")
        assert rsp.status_code == 404
        rsp = requests.delete(f"{BASE_URL}/{url}")
        assert rsp.status_code == 404

        rsp = requests.put(f"{BASE_URL}/{url}")
        assert rsp.status_code == 201
        rsp = requests.put(f"{BASE_URL}/{url}")
        assert rsp.status_code == 400

        rsp = requests.get(f"{BASE_URL}/{url}")
        assert rsp.status_code == 200

        rsp = requests.post(f"{BASE_URL}/{url}")
        assert rsp.status_code == 200
        assert rsp.json() == json_load(f"{url}")

        rsp = requests.delete(f"{BASE_URL}/{url}")
        assert rsp.status_code == 204


class TestFactorial(MathsTestCase):

    def test_requests(self):
        self.default_requests("factorial/5")


class TestFibonacci(MathsTestCase):

    def test_requests(self):
        self.default_requests("fibonacci/6")

    def test_caching(self):
        rsp = requests.put(f"{BASE_URL}/fibonacci/1000000")
        self.assertGreater(rsp.elapsed.total_seconds(), 5)
        rsp = requests.get(f"{BASE_URL}/fibonacci/1000000")
        self.assertLess(rsp.elapsed.total_seconds(), 0.1)


class TestPower(MathsTestCase):

    def test_requests(self):
        self.default_requests("power/5/6")
