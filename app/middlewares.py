# coding=utf-8

import orjson
from aiohttp import hdrs, web

from environ import REDIS_CACHE_TIME


def errors_middleware_factory():
    @web.middleware
    async def error_middleware(request, handler):
        try:
            response = await handler(request)
        except web.HTTPError as ex:
            response = web.json_response({
                "title": str(ex.reason),
                "status": ex.status_code,
                "detail": str(ex.text)
            }, status=ex.status_code)
        return response

    return error_middleware


def cache_middleware_factory(cached_endpoints):
    @web.middleware
    async def cache_middleware(request, handler):
        if request.method.lower() == "get" and request.path[1:].split("/", 1)[0] in cached_endpoints:
            response = await request.app["redis"].get(request.path)
            if not response:
                response = await handler(request)
                await request.app["redis"].set(request.path, response.text, REDIS_CACHE_TIME)
            else:
                response = web.json_response(orjson.loads(response))
        else:
            response = await handler(request)
        return response

    return cache_middleware


def cors_middleware_factory(allowed_origins: list):
    @web.middleware
    async def cors_middleware(request, handler):
        origin = request.headers.get(hdrs.ORIGIN, None)
        allowed_methods = [
            hdrs.METH_GET, hdrs.METH_POST, hdrs.METH_PATCH, hdrs.METH_DELETE, hdrs.METH_PUT, hdrs.METH_OPTIONS
        ]
        allowed_headers = [hdrs.CONTENT_TYPE]
        exposed_headers = [hdrs.CONTENT_LENGTH]
        headers = {}

        if origin:
            headers[hdrs.ACCESS_CONTROL_ALLOW_ORIGIN] = '*'
            headers[hdrs.ACCESS_CONTROL_ALLOW_CREDENTIALS] = 'true'
            headers[hdrs.ACCESS_CONTROL_MAX_AGE] = '600'

            if origin in allowed_origins:
                headers[hdrs.ACCESS_CONTROL_ALLOW_ORIGIN] = origin

            headers[hdrs.ACCESS_CONTROL_ALLOW_METHODS] = ','.join(allowed_methods)
            headers[hdrs.ACCESS_CONTROL_ALLOW_HEADERS] = ','.join(allowed_headers)
            headers[hdrs.ACCESS_CONTROL_EXPOSE_HEADERS] = ','.join(exposed_headers)

            if request.method == hdrs.METH_OPTIONS:
                return web.Response(headers=headers)

        response = await handler(request)
        if headers:
            response.headers.update(headers)
        return response

    return cors_middleware
