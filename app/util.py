# coding=utf-8

from asyncio import sleep

import asyncpg


def fibonacci(number):
    if number < 0:
        raise ValueError("The n-th Fibonacci can't be of negative index")

    fib_one, fib_two = 0, 1
    if number in [0, 1]:
        return number

    for _ in range(1, number):
        fib_one, fib_two = fib_two, fib_one + fib_two
    return fib_two


# noinspection PyBroadException
async def retry_db(_func, retries=10, interval=2):
    success = False
    for _ in range(retries):
        try:
            await _func()
        except Exception:  # pylint: disable=broad-except
            await sleep(interval)
            continue
        else:
            success = True
            break

    return success


def read_resource_file(filename):
    with open(f"res/{filename}") as file:
        output = file.read()
    return output


class DB:
    ORDER_BY = {
        "factorial": "number",
        "fibonacci": "number",
        "power": "base,exponent"
    }

    @staticmethod
    def where(start_at=1, **conditions):
        if conditions:
            where = " WHERE " + " AND ".join(f"{key} = ${idx}" for idx, key in enumerate(conditions, start_at))
            return where
        return ""

    @staticmethod
    def order_by(key: str = None):
        response = ""
        if key:
            def ord_gen():
                for value in key.split(","):
                    idx = 1 if value.startswith("-") else 0
                    yield f" {value[idx:]} {'DESC' if idx else 'ASC'}"

            response = " ORDER BY " + ", ".join(ord_gen())
        return response

    @staticmethod
    def insert_values(fields, start_at=1):
        return ", ".join(f"${idx}" for idx, _ in enumerate(fields, start_at))

    @staticmethod
    def set(start_at=1, **values):
        if values:
            return ", ".join(f"{key} = ${idx}" for idx, key in enumerate(values, start_at))
        return ""


class PostgresContextManager:
    def __init__(self, dsn):
        self.__dsn = dsn
        self.__pool = None
        self.__client = None

    async def __aenter__(self):
        self.__pool = await asyncpg.create_pool(self.__dsn)
        self.__client = await self.__pool.acquire()
        return self.__client

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.__client.close()
        await self.__pool.close()
