# coding=utf-8
import os

REDIS_HOST = os.getenv("REDIS_HOST", "redis")
REDIS_PORT = int(os.getenv("REDIS_PORT", "6379"))
REDIS_CACHE_TIME = int(os.getenv("REDIS_CACHE_TIME", "1800"))

POSTGRES_HOST = os.getenv("POSTGRES_HOST", "db")
POSTGRES_PORT = int(os.getenv("POSTGRES_PORT", "5432"))
POSTGRES_DB = os.getenv("POSTGRES_DB", "maths")
POSTGRES_USER = os.getenv("POSTGRES_USER", "maths")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
