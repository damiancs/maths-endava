# coding=utf-8
from math import factorial

from aiohttp import web
from aiohttp.web_exceptions import HTTPBadRequest, HTTPNotFound
from asyncpg import UniqueViolationError

from util import fibonacci

# pylint: disable=too-few-public-methods
class ListView(web.View):
    async def get(self):
        table = self.request.path[1:]
        page = int(self.request.query.get("page", "0"))
        per_page = int(self.request.query.get("per_page", "10"))
        rows = await self.request.app["db"].list(table, per_page=per_page, page=page)
        return web.json_response([dict(row) for row in rows])


class FactorialCalcView(web.View):

    # Create
    async def put(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].select("factorial", order_by="number", number=number)
        if row:
            raise HTTPBadRequest()

        response = {
            "number": number,
            "factorial": str(factorial(number))
        }
        try:
            await self.request.app["db"].insert(
                "factorial", ["number", "factorial"], (number, response["factorial"])
            )
        except UniqueViolationError as ex:
            raise HTTPBadRequest() from ex
        return web.json_response(response, status=201)

    # Read
    async def get(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].select("factorial", order_by="number", number=number)
        if not row:
            raise HTTPNotFound()
        response = {
            "number": number,
            "factorial": row[0]["factorial"]
        }
        return web.json_response(response)

    # Update
    async def post(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].select("factorial", order_by="number", number=number)
        if not row:
            raise HTTPNotFound()

        response = str(factorial(number))
        row = await self.request.app["db"].update("factorial", {"factorial": response}, number=number)
        if not row:
            raise HTTPNotFound()
        return web.json_response(dict(row[0]))

    # Delete
    async def delete(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].delete("factorial", number=number)
        if not row:
            raise HTTPNotFound()
        return web.json_response({}, status=204)


class FibonacciCalcView(web.View):

    # Create
    async def put(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].select("fibonacci", order_by="number", number=number)
        if row:
            raise HTTPBadRequest()

        response = {
            "number": number,
            "fibonacci": str(fibonacci(number))
        }
        try:
            await self.request.app["db"].insert(
                "fibonacci", ["number", "fibonacci"], (number, response["fibonacci"])
            )
        except UniqueViolationError as ex:
            raise HTTPBadRequest() from ex
        return web.json_response(response, status=201)

    # Read
    async def get(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].select("fibonacci", order_by="number", number=number)
        if not row:
            raise HTTPNotFound()
        response = {
            "number": number,
            "fibonacci": row[0]["fibonacci"]
        }
        return web.json_response(response)

    # Update
    async def post(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].select("fibonacci", order_by="number", number=number)
        if not row:
            raise HTTPNotFound()

        response = str(fibonacci(number))
        row = await self.request.app["db"].update("fibonacci", {"fibonacci": response}, number=number)
        if not row:
            raise HTTPNotFound()
        return web.json_response(dict(row[0]))

    # Delete
    async def delete(self):
        number = int(self.request.match_info["number"])
        row = await self.request.app["db"].delete("fibonacci", number=number)
        if not row:
            raise HTTPNotFound()
        return web.json_response({}, status=204)


class PowerCalcView(web.View):

    # Create
    async def put(self):
        base, exp = int(self.request.match_info['base']), int(self.request.match_info['exp'])
        row = await self.request.app["db"].select("power", order_by="base,exponent", base=base, exponent=exp)
        if row:
            raise HTTPBadRequest()

        response = {
            "base": base,
            "exponent": exp,
            "power": str(base ** exp)
        }
        try:
            await self.request.app["db"].insert(
                "power", ["base", "exponent", "power"], (base, exp, response["power"])
            )
        except UniqueViolationError as ex:
            raise HTTPBadRequest() from ex
        return web.json_response(response, status=201)

    # Read
    async def get(self):
        base, exp = int(self.request.match_info['base']), int(self.request.match_info['exp'])
        row = await self.request.app["db"].select("power", order_by="base,exponent", base=base, exponent=exp)
        if not row:
            raise HTTPNotFound()
        response = {
            "base": base,
            "exponent": exp,
            "power": row[0]["power"]
        }
        return web.json_response(response)

    # Update
    async def post(self):
        base, exp = int(self.request.match_info['base']), int(self.request.match_info['exp'])
        row = await self.request.app["db"].select("power", order_by="base,exponent", base=base, exponent=exp)
        if not row:
            raise HTTPNotFound()

        response = str(base ** exp)
        row = await self.request.app["db"].update("power", {"power": response}, base=base, exponent=exp)
        if not row:
            raise HTTPNotFound()
        return web.json_response(dict(row[0]))

    # Delete
    async def delete(self):
        base, exp = int(self.request.match_info['base']), int(self.request.match_info['exp'])
        row = await self.request.app["db"].delete("power", base=base, exponent=exp)
        if not row:
            raise HTTPNotFound()
        return web.json_response({}, status=204)
