# coding=utf-8
from aiohttp.web_app import Application
from aioredis import Redis

from environ import POSTGRES_DB, POSTGRES_HOST, POSTGRES_PASSWORD, POSTGRES_PORT, POSTGRES_USER, REDIS_HOST, REDIS_PORT
from exceptions import ServiceException
from util import DB, PostgresContextManager, read_resource_file, retry_db


class RedisDB:
    def __init__(self, host=REDIS_HOST, port=REDIS_PORT, db=0):
        self.redis = Redis(host=host, port=port, db=db, decode_responses=True)

    async def get(self, key):
        async with self.redis.client() as conn:
            result = await conn.get(key)
        return result

    async def set(self, key, value, seconds=None):
        async with self.redis.client() as conn:
            result = await conn.set(key, value, ex=seconds)
        return result

    async def close(self):
        await self.redis.close()


class PostgresDB:
    # pylint: disable=too-many-arguments
    def __init__(self,
                 host=POSTGRES_HOST, port=POSTGRES_PORT,
                 user=POSTGRES_USER, password=POSTGRES_PASSWORD,
                 database=POSTGRES_DB
                 ):
        self.dsn = f"postgres://{user}:{password}@{host}:{port}/{database}"
        self.connection = PostgresContextManager

    async def close(self):
        pass

    async def select(self, table, order_by=None, limit=1, offset=0, **conditions):
        if not order_by:
            order_by = DB.ORDER_BY.get("table")
        query = f"SELECT * FROM {table} " \
                + DB.where(**conditions) \
                + DB.order_by(order_by) \
                + f" LIMIT {limit} OFFSET {offset} ;"

        async with self.connection(self.dsn) as conn:
            if conditions:
                result = await conn.fetch(query, *list(conditions.values()))
            else:
                result = await conn.execute(query)
        return result

    async def insert(self, table, fields=None, values=None):
        if not fields or not values:
            return []

        query = f"INSERT INTO {table} ({', '.join(str(field) for field in fields)}) VALUES (" \
                + DB.insert_values(fields) \
                + ") RETURNING * ;"

        async with self.connection(self.dsn) as conn:
            result = await conn.fetch(query, *values)
        return result

    async def delete(self, table, **conditions):
        query = f"DELETE FROM {table} " \
                + DB.where(**conditions) \
                + " RETURNING * ;"

        async with self.connection(self.dsn) as conn:
            result = await conn.fetch(query, *list(conditions.values()))
        return result

    async def update(self, table, values: dict = None, **conditions):
        query = f"UPDATE {table} SET " \
                + DB.set(**values) \
                + DB.where(len(values) + 1, **conditions) \
                + " RETURNING * ;"

        async with self.connection(self.dsn) as conn:
            result = await conn.fetch(query, *list(values.values()), *list(conditions.values()))
        return result

    async def list(self, table, order_by=None, per_page=10, page=0, **conditions):
        if not order_by:
            order_by = DB.ORDER_BY.get("table")
        query = f"SELECT * FROM {table} " \
                + DB.where(**conditions) \
                + DB.order_by(order_by) \
                + f" LIMIT {per_page} OFFSET {page * per_page} ;"

        async with self.connection(self.dsn) as conn:
            if conditions:
                result = await conn.fetch(query, *list(conditions.values()))
            else:
                result = await conn.fetch(query)
        return result


async def make_migrations(database):
    async with database.connection(database.dsn) as conn:
        await conn.execute(read_resource_file("init_db.sql"))


async def init_dbs(app: Application):
    async def test_redis():
        _redis = RedisDB()
        await _redis.close()

    async def test_postgres():
        _db = PostgresDB()
        result = 2 ** 10
        async with _db.connection(_db.dsn) as conn:
            assert result == await conn.fetchval("SELECT 2 ^ $1", 10), "We have some issues with Postgres"

    redis = await retry_db(test_redis)
    app.logger.info(" Redis is available")

    postgres = await retry_db(test_postgres)
    app.logger.info(" Postgres is available")

    if not redis or not postgres:
        raise ServiceException("Database (redis or postgres) is not accessible")

    if redis:
        app["redis"] = RedisDB()

    if postgres:
        app["db"] = PostgresDB()

    await make_migrations(app["db"])


async def close_dbs(app: Application):
    if "redis" in app and app["redis"]:
        await app["redis"].close()

    if "db" in app and app["db"]:
        await app["db"].close()
