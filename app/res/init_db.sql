DROP TABLE IF EXISTS factorial;
CREATE TABLE IF NOT EXISTS factorial
(
    number    int
        constraint factorial_pk primary key,
    factorial text not null
);
DROP TABLE IF EXISTS fibonacci;
CREATE TABLE IF NOT EXISTS fibonacci
(
    number    int
        constraint fibonacci_pk primary key,
    fibonacci text not null
);
DROP TABLE IF EXISTS power;
CREATE TABLE IF NOT EXISTS power
(
    base     int,
    exponent int,
    power    text not null,
    CONSTRAINT base_exponent PRIMARY KEY (base, exponent)
);