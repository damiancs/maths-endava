# Maths - test for Endava

![pipeline](https://gitlab.com/damiancs/maths-endava/badges/master/pipeline.svg)
![pylint](https://gitlab.com/damiancs/maths-endava/-/jobs/artifacts/master/raw/pylint.svg?job=lint)
![pytest](https://gitlab.com/damiancs/maths-endava/-/jobs/artifacts/master/raw/pytest.svg?job=test)

## Components
Our application is started as a docker container using the image that we build based on our docker recipe and using a few additional services:
* `redis` is a service that serves for caching requests. It's based on the `redis:alpine` docker image.
* `db` is a service that serves as a permanent database. It's based on the `postgres:alpine` docker image.
* `swagger` is a service that describes the available APIs. It's based on the `swaggerapi/swagger-ui` docker image.
* `web` is the service written by me, which uses the following:
  * `python 3.9` as the programming language;
  * `aiohttp` as an asynchronous web framework;
  * `aioredis` for connecting to `redis`;
  * `asyncpg` for connecting to `postgres`;
  * `orjson` as the fastest json library available for `python`.

## Running the application
### Prerequisites
In order to run the application you need to have the following installed on your system:
* `docker` to build our image;
* `docker-compose` to download and link all the needed services.

### Steps
0. Position yourself in the main directory for any of the following commands;
1. Run `docker-compose build` so you can build the image on your local machine;
2. Run `docker-compose up` to get all the services up and running;
   * use `docker-compose up -d` in order to run it in background (detached mode);
3. **When you want to stop everything** just run `docker-compose down`.

## Testing the application
I wrote a few tests for the application and included them in the Gitlab's pipeline so if a test is failing the pipeline badge will turn red. If the pipeline is green then all the tests are passed.
