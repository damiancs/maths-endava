FROM python:3.9-slim
LABEL maintainer="Constantin-Sebastian Dămian <owner@damiancs.ro>"

COPY *.deps /
RUN python -m pip install --no-cache-dir --no-warn-script-location --user -r runtime.deps

WORKDIR /app
COPY ./app/*.py /app/

ENTRYPOINT ["python", "main.py"]
EXPOSE 80/tcp
